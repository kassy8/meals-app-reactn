import React from 'react';
import { Platform } from 'react-native';
import {HeaderButton} from 'react-navigation-header-buttons';
import {Ionicons,FontAwesome,AntDesign} from '@expo/vector-icons';

import Colors from '../constants/Colors';

export const IconButtonAnt = (props) => {
    return(<HeaderButton {...props} 
    IconComponent={AntDesign} 
    iconSize={23} color={'white'} />);
    //(Platform.OS==='android') ? Colors.accentColor : 'white'
}

export const IconButtonFA = (props) => {
    return(<HeaderButton {...props} 
    IconComponent={FontAwesome} 
    iconSize={23} color={'white'} />);
    //(Platform.OS==='android') ? Colors.accentColor : 'white'
}

export const IconButtonIon = (props) => {
    return(<HeaderButton {...props} 
    IconComponent={Ionicons} 
    iconSize={23} />);
    //(Platform.OS==='android') ? Colors.accentColor : 'white'
}


