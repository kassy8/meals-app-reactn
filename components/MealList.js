import React from 'react';
import {View,StyleSheet,FlatList} from 'react-native';
import MealItem from '../components/MealItem';



const MealList = ({listData,navigation}) => {
    
    const renderMealItem = (listData) => {
        return (
            <MealItem 
            {...listData} 
            onSelectMeal={() => navigation.navigate({
                routeName: 'MealDetail', 
                params: {
                    mealId: listData.item.id,
                    mealDetails: listData.item
                } 
            })} 
            />
        );
    }

    return(
        <View style={styles.list}>
            <FlatList 
            keyExtractor={(item,index) => item.id } 
            data={listData}
            renderItem={renderMealItem} 
            numColumns={1} 
            style={{width: '100%'}}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    list: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default MealList;