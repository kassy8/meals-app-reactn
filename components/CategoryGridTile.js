import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform, TouchableNativeFeedback} from 'react-native';

const CategoryGridTile = (props) => {
    let TouchableCmp = TouchableOpacity;
    if(Platform.OS == 'android' && Platform.Version >= 21){
        TouchableCmp= TouchableNativeFeedback
    }
    return (
        <View style={styles.gridItem}>
        <TouchableCmp style={{flex: 1}} onPress={props.onSelect}>
            <View style={{...styles.container,...{backgroundColor: props.bgColor}}}>
                <Text style={styles.title} numberOfLines={2}>{props.title}</Text>
            </View>
        </TouchableCmp>
        </View>
    )
}

const styles = StyleSheet.create({
    gridItem: {
        flex: 1,
        justifyContent: 'center',
        margin: 15,
        height: 150,
        borderRadius: 15,
        overflow:"hidden"
    },
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        borderRadius: 15,
        padding: 10,

        shadowOpacity: 0.26,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 3},
        shadowRadius: 15,
        elevation: 4
    },
    title:{
        fontSize: 22,
        fontFamily: 'open-sans-bold',
        textAlign: 'right'
    }
});


export default CategoryGridTile;