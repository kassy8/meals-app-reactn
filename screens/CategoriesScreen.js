import React from 'react';
import {View, Text, StyleSheet, FlatList } from 'react-native';
import CategoryGridTile from '../components/CategoryGridTile';

import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import {IconButtonFA,IconButtonAnt,IconButtonIon} from '../components/HeaderButton';

import {CATEGORIES} from '../data/dummy-data';

const CategoriesScreen = (props) => {
    
    const renderGridItem = (itemData) => {
        return (
            <CategoryGridTile 
                title={itemData.item.title}
                bgColor={itemData.item.color}
                onSelect={() => props.navigation.navigate({
                    routeName: 'CategoryMeals', 
                    params: {
                        categoryId: itemData.item.id
                    } 
                })} 
            />
        )
    }

    return(
        <View>
            <FlatList 
            keyExtractor={(item,index) => item.id } 
            data={CATEGORIES} 
            renderItem={renderGridItem} 
            numColumns={2} />
        </View>
    );
}; 

CategoriesScreen.navigationOptions = (navData) => {
    return {
        headerTitle: 'Meal Categories',
        headerLeft: (
        <HeaderButtons HeaderButtonComponent={IconButtonIon}>
            <Item 
            title='Menu' 
            iconName='ios-menu' 
            onPress={()=>{
                navData.navigation.toggleDrawer()
            }}
            />
        </HeaderButtons>
        )
    };
};


const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default CategoriesScreen;