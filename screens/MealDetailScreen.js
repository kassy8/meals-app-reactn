import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView } from 'react-native';

import {CATEGORIES,MEALS} from '../data/dummy-data';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import {IconButtonFA,IconButtonAnt,IconButtonIon} from '../components/HeaderButton';

const MealDetailsScreen = (props) => {
    
/*
ALTERNATIVES:
-----
//get the WHOLE meal object
    const mealdetail= props.navigation.getParam('mealDetails');
|*/
//find the meal object
    const mealId= props.navigation.getParam('mealId');
    const mealdetail= MEALS.find((meal) => meal.id==mealId);

    return(
        <View style={styles.mealItem}>
            <Image style={styles.bgImage} source={{uri: mealdetail.imageUrl}} />
            <ScrollView style={styles.scrollContent}>
                <View>
                    <Text>{mealdetail.title}</Text>
                    <Text>{mealdetail.affordability}</Text>
                    <Text>{mealdetail.complexity}</Text>
                    <Text>{mealdetail.imageUrl}</Text>
                    <Text>{mealdetail.duration}</Text>
                    <Text>Ingredients</Text>
                    {mealdetail.ingredients.map((ingredient,i)=>
                        <Text key={`in${i++}`}>{`${i}. ${ingredient}`}</Text>
                    )}
                    <Text>Instructions</Text>
                    {mealdetail.instructions.map((step,i)=>
                        <Text key={`st${i++}`}>{ `${i}. ${step}` }</Text>
                    )}
                    <Text>Dietry</Text>
                    <Text>Gluten Free? {(mealdetail.isGlutenFree) ? "TICK!":"Nope" }</Text>
                    <Text>Vegetarian? {(mealdetail.isVegetarian) ? "TICK!":"Nope" }</Text>
                    <Text>Lacto Free? {(mealdetail.isLactoseFree) ? "TICK!":"Nope" }</Text>
                    <Text>Vegan? {(mealdetail.isVegan) ? "TICK!":"Nope" }</Text>
                </View>
            </ScrollView>
        </View>
    );
};


MealDetailsScreen.navigationOptions = (navigationData) => {
    
    const mealId= navigationData.navigation.getParam('mealId');
    const mealdetail= MEALS.find((meal) => meal.id==mealId);
    return{
        headerTitle: mealdetail.title,
        headerRight: (<HeaderButtons HeaderButtonComponent={IconButtonIon}>
            <Item 
            title='Favourite' 
            iconName='md-star-outline' 
            onPress={()=>{console.log('This is a FAVE')}} 
            />
        </HeaderButtons>)
    }
};


const styles = StyleSheet.create({
    mealItem: {
        width: '100%',
        backgroundColor: '#f5f5f5',
        height: '100%'
    },
    bgImage:{
        width: '100%',
        height: '30%'
    },
    scrollContent: {
        width: '100%',
        height: '70%',
        overflow:"hidden"
    }
});

export default MealDetailsScreen;

            /**/