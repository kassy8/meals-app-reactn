import React, {useState,useEffect,useCallback} from 'react';
import {View, Text, StyleSheet, Switch,Platform } from 'react-native';

import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import {IconButtonFA,IconButtonAnt,IconButtonIon} from '../components/HeaderButton';
import Colors from '../constants/Colors';


const FilterSwitch = (props) => {
    return (
        <View style={styles.filterContainer}>
            <Text>{props.label}</Text>
            <Switch 
            value={props.state}
            onValueChange={props.onChange} 
            trackColor={{true: Colors.primaryColor}} 
            thumbColor={Platform.OS==='android' ? Colors.accentColor:''}
            />
        </View>
    );
}


const FiltersScreen = (props) => {

    const {navigation} = props;

    const [isLactoseFree, setIsLactoseFree] = useState(false);
    const [isVegitarian, setIsVegitarian] = useState(false);
    const [isGlutenFree, setIsGlutenFree] = useState(false);
    const [isVegan, setIsVegan] = useState(false);

    const saveFilters = useCallback(()=>{
        const appliedFilters= {
            gf: isGlutenFree,
            lf: isLactoseFree,
            vg: isVegitarian,
            va: isVegan
        };
        console.log(appliedFilters);
    },[isLactoseFree,isVegitarian,isGlutenFree,isVegan]);

    useEffect(()=>{
        props.navigation.setParams({save: saveFilters})
    },[saveFilters]);

    return(
        <View style={styles.screen}>
            <Text>The FiltersScreen Screen!</Text>
            <FilterSwitch label='Lactose-free' state={isLactoseFree} 
            onChange={(newVal) => setIsLactoseFree(newVal)} />
            <FilterSwitch label='Vegetarian' state={isVegitarian} 
            onChange={(newVal) => setIsVegitarian(newVal)} />
            <FilterSwitch label='Gluten Free' state={isGlutenFree} 
            onChange={(newVal) => setIsGlutenFree(newVal)} />
            <FilterSwitch label='Vegan' state={isVegan} 
            onChange={(newVal) => setIsVegan(newVal)} />
        </View>
    );
};

FiltersScreen.navigationOptions= (navData) => {
    return {
        headerTitle: 'Filter a Ting fam',
        headerLeft: (
        <HeaderButtons HeaderButtonComponent={IconButtonIon}>
            <Item 
            title='Menu' 
            iconName='ios-menu' 
            onPress={()=>{
                navData.navigation.toggleDrawer()
            }}
            />
        </HeaderButtons>
        ),
        headerRight: (
        <HeaderButtons HeaderButtonComponent={IconButtonIon}>
            <Item 
            title='Save' 
            iconName='ios-save' 
            onPress={
                navData.navigation.getParam('save')
            }
            />
        </HeaderButtons>
        ),
    };
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center'
    },
    filterContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '90%',
        marginTop: 3
    }
});

export default FiltersScreen;