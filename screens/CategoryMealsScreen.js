import React from 'react';

import {CATEGORIES,MEALS} from '../data/dummy-data';
import MealList from '../components/MealList';


const CategoryMealsScreen = (props) => {

    const catId= props.navigation.getParam('categoryId');
    const mealsincluded= MEALS.filter(
        meal => meal.catIds.indexOf(catId) >= 0
    );

    return(<MealList listData={mealsincluded} navigation={props.navigation} />);
};

CategoryMealsScreen.navigationOptions = (navigationData) => {
    
    const catId= navigationData.navigation.getParam('categoryId');
    const cat= CATEGORIES.find((cat) => cat.id==catId);
    return{
        headerTitle: `Elia's ${cat.title} Dishes`
    }
};


export default CategoryMealsScreen;