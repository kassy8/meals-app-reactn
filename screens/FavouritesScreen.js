import React from 'react';
import MealList from '../components/MealList';
import {MEALS} from '../data/dummy-data';

import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import {IconButtonFA,IconButtonAnt,IconButtonIon} from '../components/HeaderButton';

const chosenFaves= ['m3','m8'];

const FavouritesScreen = (props) => {

    const favMeals = MEALS.filter(
        meal => chosenFaves.indexOf(meal.id) >= 0
        //meal.catIds.indexOf(chosenFaves) >= 0
    );
    return(<MealList listData={favMeals} navigation={props.navigation} />);
};

FavouritesScreen.navigationOptions= (navData) => {
    return {
        headerTitle: 'Your Favourites',
        headerLeft: (
        <HeaderButtons HeaderButtonComponent={IconButtonIon}>
            <Item 
            title='Menu' 
            iconName='ios-menu' 
            onPress={()=>{
                navData.navigation.toggleDrawer()
            }}
            />
        </HeaderButtons>
        )
    };
};



export default FavouritesScreen;