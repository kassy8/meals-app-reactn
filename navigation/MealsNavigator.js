import React from 'react';
import {Platform, Text} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator} from 'react-navigation-stack';
import {Ionicons,AntDesign} from '@expo/vector-icons';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';

import CategoriesScreen from '../screens/CategoriesScreen';
import CategoryMealsScreen from '../screens/CategoryMealsScreen';
import MealDetailScreen from '../screens/MealDetailScreen';
import FavouritesScreen from '../screens/FavouritesScreen';
import FiltersScreen from '../screens/FiltersScreen';
import Colors from '../constants/Colors';


const defaultStackNavOptions= {
  defaultNavigationOptions: {
    headerStyle: {
        backgroundColor: (Platform.OS=="android") ? Colors.primaryColor : Colors.accentColor,
        color:'green'
    },
    headerTitleStyle: {
      fontFamily: 'open-sans-bold'
    },
    headerBackTitleStyle: {
      fontFamily: 'open-sans',
      color: 'yellow'
    },
    headerTintColor: 'white', /*(Platform.OS=="ios") ? Colors.accentColor : Colors.primaryColor*/
    headerTitle: 'Elia\'s Kitchen'
  }
};


const MealsNavigator = createStackNavigator(
{
  Categories: CategoriesScreen,
  CategoryMeals: CategoryMealsScreen,
  MealDetail: MealDetailScreen
}, defaultStackNavOptions);

const FavNavigator = createStackNavigator({
  Favourites: FavouritesScreen,
  MealDetail: MealDetailScreen
}, defaultStackNavOptions);

const FiltersNavigator = createStackNavigator({
  Filters: FiltersScreen
}, defaultStackNavOptions);
/*{
  navigationOptions:{
    drawerLabel: 'Filter choices',
    headerBackImage: {
      color:'red'
    },
  },
  defaultNavigationOptions: defaultStackNavOptions
}*/
const tabScreenConfig= {
  Meals: {screen: MealsNavigator,navigationOptions:{
    tabBarIcon: (tabInfo)=>{
      return <Ionicons name="md-restaurant" size={25} color={tabInfo.tintColor} />
    },
    tabBarColor: Colors.accentColor,
    tabBarLabel: (Platform.OS==='android') ? <Text style={{fontFamily:'open-sans-bold'}}>Meals</Text> : 'Meals!'
  }},
  Favourites: {screen: FavNavigator,navigationOptions:{
    tabBarLabel: 'My Faves!',
    tabBarIcon: (tabInfo)=>{
      return <AntDesign name="star" size={25} color={tabInfo.tintColor} />
    },
    tabBarColor: 'blue',
    tabBarLabel: (Platform.OS==='android') ? <Text style={{fontFamily:'open-sans-bold'}}>Favourites</Text> : 'Favourites!'
  }}
};

const MealsFaveTabNav = (Platform.OS==='android') ? 
createMaterialBottomTabNavigator(tabScreenConfig,{
  activeColor: 'white',
  shifting: true, //MUST be set to false if using barStyle below
  barStyle:{
    backgroundColor: Colors.primaryColor
  }
})
:
createBottomTabNavigator(tabScreenConfig,{
  tabBarOptions:{
    labelStyle: {
      fontFamily: 'open-sans-bold'
    },
    activeTintColor: 'white',
    style: {
      backgroundColor: Colors.primaryColor
    }
  }
});


const MainNavigator = createDrawerNavigator({
  MealsFavs: {screen: MealsFaveTabNav, navigationOptions: {
    drawerLabel: 'Meals'
  }},
  Filters: FiltersNavigator,
}, {
  contentOptions: {
    labelStyle: {
      fontFamily: 'open-sans'
    }
  }
});

export default createAppContainer(MainNavigator);
